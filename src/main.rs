mod selector;

fn main() {
    let (sender, receiver) = flume::bounded(8);

    let mut signals: Vec<flume::Sender<()>> = Vec::new();

    let mut launch = true;

    for i in 0u64.. {
        if i % 100000 == 0 {
            println!("looping");
        }

        if i % 10000 == 0 {
            if signals.len() >= 3 || !launch {
                launch = false;

                if let Some(signal_tx) = signals.pop() {
                    signal_tx.send(()).expect("Sent");
                    drop(signal_tx);
                }
            }

            if signals.len() <= 1 || launch {
                launch = true;

                let (signal_tx, signal) = flume::bounded(1);

                signals.push(signal_tx);

                let rx2 = receiver.clone();
                std::thread::spawn(move || {
                    println!("Launching thread");
                    while !race(&rx2, &signal) {
                        // spin
                    }
                    println!("Closing thread");
                });
            }
        }

        let (dropper_tx, dropper) = flume::bounded(1);

        sender.send(Dropper { sender: dropper_tx }).expect("sent");

        dropper.recv().expect("received");
    }
}

struct Dropper {
    sender: flume::Sender<()>,
}

impl Drop for Dropper {
    fn drop(&mut self) {
        self.sender.send(()).expect("sent");
    }
}

/* working selector
 * fn race(receiver: &flume::Receiver<Dropper>, signal: &flume::Receiver<()>) -> bool {
 *     match selector::blocking_select(receiver.recv_async(), signal.recv_async()) {
 *         selector::Either::Left(res) => {
 *             let out = res.is_err();
 *             drop(res);
 *             out
 *         }
 *         selector::Either::Right(_res) => true,
 *     }
 * }
 */

// broken selector
fn race(receiver: &flume::Receiver<Dropper>, signal: &flume::Receiver<()>) -> bool {
    flume::Selector::new()
        .recv(receiver, |res| {
            let out = res.is_err();
            drop(res);
            out
        })
        .recv(signal, |_res| true)
        .wait()
}
